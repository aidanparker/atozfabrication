<?php $pageTitle="Services"; include 'lib/inc/header.php';?>
        
                <section class="sale">
                    <header>
                        <h3>Services</h3>
                        <h4><a href="contact.php" title="Contact us about your next project!">Contact us about your next project!</a></h4>
                    </header>
                    <figure class="service">
                        <img src="lib/img/services/proinstalls.jpg" title="Professional Installations" alt="Professional Installations">
                        <figcaption>
                        <strong>Professional Installations</strong>
                        Lift kits, axle installations, body protection and armor &mdash; you name it, we'll install it. And we'll install it right.
                        </figcaption>
                    </figure>
                    <figure class="service alt">
                        <img src="lib/img/services/axlesandlockers.jpg" title="Axels and Lockers" alt="Axels and Lockers">
                        <figcaption>
                        <strong>Axels and Lockers</strong>
                        AtoZ’s got your covered for axle regearing and locker installations. Custom axle builds, trussing, disc brake swaps? We can do it all.
                        </figcaption>
                    </figure>                    
                    <figure class="service">
                        <img src="lib/img/services/rollcages.jpg" title="Roll Cages" alt="Roll Cages">
                        <figcaption>
                        <strong>Roll Cages</strong>
                        From sport cage installations to
full custom competition cages for vehicles of all kinds &mdash; we can cut
it, notch it, bend it and weld it strong and stout!
                        </figcaption>
                    </figure>                   
                    <figure class="service alt">
                        <img src="lib/img/services/customultra.jpg" title="Custom Ultra 4 and Rock Crawlers" alt="Custom Ultra 4 and Rock Crawlers">
                        <figcaption>
                        <strong>Custom Ultra 4 and Rock Crawlers</strong>
                        The sky is the limit. We’ve built custom Ultra4 race cars 
and competition crawlers and rock bouncers. If you can dream it wecan fabricate it.
                        </figcaption>
                    </figure>                    
                    <figure class="service">
                        <img src="lib/img/services/cncplasma.jpg" title="CNC Plasma Cutting" alt="CNC Plasma Cutting">
                        <figcaption>
                        <strong>CNC Plasma Cutting</strong>
We can handle precision CNC Plasma cutting up to 1/2" thick steel plate. It doesn’t have to be related to your 4×4, we were just born to cut steel!
                        </figcaption>
                    </figure>                    
                    <figure class="service alt">
                        <img src="lib/img/services/tubebending.jpg" title="Tube Bending" alt="Tube Bending">
                        <figcaption>
                        <strong>Tube Bending</strong>
We can handle bending tube from 3/4" to 3" on our benders. Cages, custom projects, hadrails &mdash; whatever you can dream up.
                        </figcaption>
                    </figure>                    
                    <figure class="service">
                        <img src="lib/img/services/generalrepairs.jpg" title="General Repairs" alt="General Repairs">
                        <figcaption>
                        <strong>General Repairs</strong>
AtoZ Fabrication can do more than just cut and weld metal. We do vehicle service, general repairs, and parts installations. Maybe you don’t know how or you’re just too busy to install that clutch or water pump yourself.
                        </figcaption>
                    </figure>                    
                    <figure class="service alt">
                        <img src="lib/img/services/framerest.jpg" title="Frame Restoration and Repair" alt="Frame Restoration and Repair">
                        <figcaption>
                        <strong>Frame Restoration and Repair</strong>
If you’re from the north east like we are you know about vehicle rust. If you have a rusted frame or unit body we can weld you back together right and keep the rust at bay. The backbone of your vehicle needs to be strong &mdash; let us help keep it that way.
                        </figcaption>
                    </figure>
                </section>
                
<?php include 'lib/inc/footer.php';?>