<?php $pageTitle="Contact"; include 'lib/inc/header.php';?>
        
                <section class="sale">
                    <header>
                        <h3>Contact</h3>
                    </header>
                    <p>
                    <address>
                        <strong>AtoZ Fabrication</strong><br>
                        15 Saint Charles St.<br>
                        Schuylkill Haven, PA 17972<br>
                        (570) 385–3931
                    </address>
                    </p>
                    <form class="contact">
                        <fieldset class="required">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" placeholder="John Smith" required>
                        </fieldset>
                        <fieldset class="required">
                            <label for="email">Email Address</label>
                            <input type="email" name="email" id="email" placeholder="example@example.com" required>
                        </fieldset>
                        <fieldset>
                            <label for="phone">Phone Number</label>
                            <input type="tel" name="phone" id="phone" placeholder="(###) ###&ndash;####">
                        </fieldset>
                        <fieldset class="required">
                            <label for="topic">Topic</label>
                            <select name="topic" id="topic" required>
                                <option value="">Appointment</option>
                                <option value="asupport">Support</option>
                                <option value="feedback">Feedback</option>
                                <option value="other">Other</option>
                            </select>
                        </fieldset>
                        <fieldset class="required full">
                            <label for="message">Message</label>
                            <textarea name="message" id="message" placeholder="Enter your message here&hellip;" required></textarea>
                        </fieldset>
                        <fieldset class="submitfield full">
                            <input type="submit" class="submitbutton" value="Submit &raquo;" title="Submit">
                        </fieldset>
                    </form>
                </section>
                
<?php include 'lib/inc/footer.php';?>