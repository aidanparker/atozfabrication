<!DOCTYPE html>
<html>
    <head>
        <title>AtoZ Fabrication | <?php echo $pageTitle ?></title>
        <meta name="description" content="AtoZ Fabrication is a fabrication shop specializing in Jeep and related parts, located in Schuylkill Haven, Pennsylvania"/>
        <meta name="keywords" content="atoz, fabrication, jeep, wrangler, cars, outdoors, pennsylvania, united states, america, schuylkill haven"/>
        <meta charset="utf-8"/>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="lib/css/reset.css"/>
        <link rel="stylesheet" type="text/css" href="lib/css/styles.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="icon" type="image/png" href="lib/img/site/favicon.png" />
    </head>
    <body>
        <div id="container">
            <header id="mainheader">
                <h1><a href="index.php" title="AtoZ Fabrication"><img src="lib/img/site/Logo.svg" title="AtoZ Fabrication" alt="AtoZ Fabrication"></a></h1>
                <div class="usersocial">
                    <div class="user">
                        <ul>
                            <li><a href="#" title="Sign in">Sign in</a></li>
                            <li><a href="#" title="Register">Register</a></li>
                            <li><a href="#" title="You have 0 items in your cart." class="cart"><i class="fa fa-shopping-cart"></i>&nbsp;0 Items</a></li>
                        </ul>
                    </div>
                    <div class="sm">
                        <ul>
<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <nav id="mainnav">
                <ul>
                    <li><a href="#" title="Products">Products</a></li>
                    <li><a href="#" title="Shop by Vehicle">Shop by Vehicle</a></li>
                    <li><a href="services.php" title="Services">Services</a></li>
                    <li><a href="about.php" title="Our Story">Our Story</a></li>
                    <li><a href="contact.php" title="Contact">Contact</a></li>
                </ul>
                <form>
                    <input type="search" placeholder="Search&hellip;"><i class="fa fa-search" title="Search"></i>
                    <!--<input type="submit" value="Search" title="Search">-->
                </form>
            </nav>
            <div id="innercontainer">