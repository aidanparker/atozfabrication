            </div>
                <aside id="categories">
                    <header><h2>Shop by Category</h2></header>
                    <ul>
                        <li><a href="#" title="Builder Parts">Builder Parts</a></li>
                        <li><a href="#" title="Suspension and Lift Kits">Suspension and Lift Kits</a></li>
                        <li><a href="#" title="Rocker Guards">Rocker Guards</a></li>
                        <li><a href="#" title="Winch and Recovery">Winch and Recovery</a></li>
                        <li><a href="#" title="Bumpers">Bumpers</a></li>
                        <li><a href="#" title="Skid Plates">Skid Plates</a></li>
                        <li><a href="#" title="Body Armor">Body Armor</a></li>
                        <li><a href="#" title="Tube Fenders">Tube Fenders</a></li>
                        <li><a href="#" title="Frame Reinforcement and Repair">Frame Reinforcement and Repair</a></li>
                        <li><a href="#" title="Axles and Driveline">Axles and Driveline</a></li>
                        <li><a href="#" title="Differential Protection">Differential Protection</a></li>
                        <li><a href="#" title="Sport Cages">Sport Cages</a></li>
                        <li><a href="#" title="Wheels and Accessories">Wheels and Accessories</a></li>
                        <li><a href="#" title="Steering">Steering</a></li>
                        <li><a href="#" title="Brakes">Brakes</a></li>
                        <li><a href="#" title="Exterior Accessories">Exterior Accessories</a></li>
                    </ul>
                    <hr>
                    <div class="twitter-container">
                        <a class="twitter-timeline" href="https://twitter.com/AtoZFabrication" data-widget-id="659487306676633600">Tweets by @AtoZFabrication</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </div>
                </aside>
            <footer id="mainfooter">
            <div class="footerlist">
                                <h1><a href="index.php" title="AtoZ Fabrication"><img src="lib/img/site/LogoReverse.svg" title="AtoZ Fabrication" alt="AtoZ Fabrication"></a></h1>
                 <form>
                    <fieldset>
                        <legend>Mailing List Signup</legend>
                        <input type="email" placeholder="Enter your email address&hellip;">
                        <i class="fa fa-plus" title="Subscribe"></i>
                     </fieldset>
                 </form>
                </div>
                <div class="footerlist">
                    <strong>Shop Secure</strong>
                    <ul>
                        <li><a href="#" title="Products">Products</a></li>
                        <li><a href="#" title="My Orders">My Orders</a></li>
                        <li><a href="#" title="Return Policy">Return Policy</a></li>
                        <li><a href="#" title="Terms and Conditions">Terms and Conditions</a></li>
                        <li><a href="#" title="Privacy Statement">Privacy Statement</a></li>
                    </ul>
                </div>
                <div class="footerlist">
                    <strong>Navigation</strong>
                    <ul>
                        <li><a href="#" title="Shop by Vehcile">Shop by Vehcile</a></li>
                        <li><a href="#" title="Fabrication Services">Fabrication Services</a></li>
                        <li><a href="#" title="Our Story">Our Story</a></li>
                        <li><a href="#" title="Contact">Contact</a></li>
                        <li><a href="#" title="Sitemap">Sitemap</a></li>
                    </ul>
                </div>
                <div class="footerlist">
                    <strong>Customer Care</strong>
                    <p>
                        <strong>AtoZFabrication</strong><br>
                        <address>15 Saint Charles St.<br>
                        Schyulkill Haven, PA 17972
                        </address>
                    </p>
                    <p>
                        (570) 385&ndash;3931<br>
                        info@atozfabrication.com<br>
                        M&ndash;Fr | 9:00am&ndash;6:00pm<br>
                        Weekends by Appointment
                    </p>
                </div>
                <div class="footerfoot">
                    <p>&copy; AtoZ Fabrication Inc. All rights reserved. Made in the USA, and proud of it.</p>
                    <div class="smfooter">
                        <ul>
                            <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title="YouTube"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>