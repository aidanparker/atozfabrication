<?php $pageTitle= "Home"; include 'lib/inc/header.php';?>

                <!--Different pages start here-->

                <section id="landing"><header><h2>AtoZ Fabrication:<br>Made in the USA and proud of it.</h2><a href="#" title="Start Shopping">Start Shopping &raquo;</a></header></section>

                <section class="sale">
                    <header>
                        <h3>Items on Sale</h3>
                        <h4><a href="#" title="Get up to 20% off!">Get up to 20% off!</a></h4>
                    </header>
                    <figure>
                        <img src="lib/img/products/95xpwinch.jpg" alt="WARN 9.5xp Winch" title="WARN 9.5xp Winch">
                        <figcaption>
                            <p><strong>WARN 9.5xp Winch</strong>
                            <span class="price">$1294.99</span>&nbsp;(11% off)</p>
                            <a href="#" title="View Details" class="details">View Details &raquo;</a>
                        </figcaption>
                    </figure>
                    <figure>
                        <img src="lib/img/products/zeon8swinch.jpg" alt="WARN ZEON 8-S Winch" title="WARN ZEON 8-S Winch">
                        <figcaption>
                            <p><strong>WARN ZEON 8-S Winch</strong>
                            <span class="price">$1034.99</span>&nbsp;(10% off)</p>
                            <a href="#" title="View Details" class="details">View Details &raquo;</a>
                        </figcaption>
                    </figure>
                    <footer>
                        <a href="#" title="View all products on sale" class="viewall">View all products on sale&hellip;</a>
                    </footer>
                </section>

                <section class="sale">
                    <header>
                        <h3>Latest Products</h3>
                    </header>
                    <figure>
                        <img src="lib/img/products/raisedtracbar.jpg" alt="Artec Heavy Duty Raised Tracbar Bracket for Jeep Wrangler JK" title="Artec Heavy Duty Raised Tracbar Bracket for Jeep Wrangler JK">
                        <figcaption>
                            <p><strong>Artec Heavy Duty Raised Tracbar Bracket for Jeep Wrangler JK</strong>
                            <span class="price">$44.99</span></p>
                            <a href="#" title="View Details" class="details">View Details &raquo;</a>
                        </figcaption>
                    </figure>
                    <figure>
 <img src="lib/img/products/uppercontrol.jpg" alt="Artec Rear Upper Control Arm Brackets for Jeep Wrangler JK" title="Artec Rear Upper Control Arm Brackets for Jeep Wrangler JK">
                        <figcaption>
                            <p><strong>Artec Rear Upper Control Arm Brackets for Jeep Wrangler JK</strong>
                            <span class="price">$44.99</span></p>
                            <a href="#" title="View Details" class="details">View Details &raquo;</a>
                        </figcaption>
                    </figure>
                    <footer>
                        <a href="#" title="View all latest products" class="viewall">View all latest products&hellip;</a>
                    </footer>
                </section>

                <section class="sale">
                    <header>
                        <h3>Best Sellers</h3>
                    </header>
                    <figure>
                        <img src="lib/img/products/hingekit.jpg" alt="Tire Carrier Hinge Kit" title="Tire Carrier Hinge Kit">
                        <figcaption>
                            <p><strong>Tire Carrier Hinge Kit</strong>
                            <span class="price">$54.99</span></p>
                            <a href="#" title="View Details" class="details">View Details &raquo;</a>
                        </figcaption>
                    </figure>
                    <figure>
 <img src="lib/img/products/swingout.jpg" alt="Tire Carrier Swingout Builder&rsquo;s Kit" title="Tire Carrier Swingout Builder&rsquo;s Kit">
                        <figcaption>
                            <p><strong>Tire Carrier Swingout Builder&rsquo;s Kit</strong>
                            <span class="price">$44.99</span></p>
                            <a href="#" title="View Details" class="details">View Details &raquo;</a>
                        </figcaption>
                    </figure>
                    <footer>
                        <a href="#" title="View all best sellers" class="viewall">View all best sellers&hellip;</a>
                    </footer>
                </section>

                <!--Different pages end here-->

<?php include 'lib/inc/footer.php';?>