<?php $pageTitle="About"; include 'lib/inc/header.php';?>
        
                <section class="sale">
                    <header>
                        <h3>Our Story</h3>
                    </header>
                    <p>Established in 2002, AtoZ Fabrication, Inc. hailed from a one-car garage in southern New Jersey. At that time there weren’t many product offerings or custom fabricators specializing in Jeep and 4x4’s in the Northeast. AtoZ immediately started building stout, custom 4x4 equipment that held up on the trail and quickly outgrew their small garage. The decision was made to part ways with Jersey and move operations to central Pennsylvania, closer to popular off-road parks like Paragon, AOAA and the “Moab of the East Coast” - Rausch Creek. AtoZ started to expand by not only enlarging our own product line, but carrying other well-respected off-road products as well.</p>
                    <p>AtoZ strives to produce and offer products that the average enthusiast can afford and will take abuse for the life of your vehicle. Our focus is your fun and enjoyment - from the Weekend Warrior enjoying the outdoors with friends and family to an Ultra4 race car or competition buggy - we have your needs covered.</p>
                    <p>AtoZ Fabrication is located in east-central Pennsylvania—in the heart or great wheeling and great people. We offer a complete fabrication facility, including machining services, powdercoating services, on-site CAD design and CNC plasma cutting, MIG and TIG welding, and full mechanical services. We are also connected to the largest parts inventory on the planet and can get what you need usually in 1-2 days if it is not already in stock at our shop. We take extreme pride in only sending quality out of our doors. AtoZ’s focus is to ensure that our customers are happy with the projects or the products that we offer. We work for you like we would want to be treated and our happy customers are a testament to that.</p>
                  <p><strong>Zach Vaughn</strong><br>
                      Owner</p>
                </section>
                
<?php include 'lib/inc/footer.php';?>